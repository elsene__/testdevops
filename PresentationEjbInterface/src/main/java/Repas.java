import java.io.Serializable;

public class Repas implements Serializable {

    private String nom;
    private double prix;
    private Boolean dehors;


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public Boolean getDehors() {
        return dehors;
    }

    public void setDehors(Boolean dehors) {
        this.dehors = dehors;
    }

    public Repas(String nom , Double Prix, Boolean dehors){
        super();
        this.nom=nom;
        this.prix= Prix;
        this.dehors =dehors;
    }
    public Repas(){

    }
}
