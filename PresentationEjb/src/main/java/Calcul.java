import javax.ejb.Remote;
import javax.ejb.Singleton;

@Remote(CalculInterface.class)
@Singleton
public class Calcul implements CalculInterface {
    @Override
    public int ajouter(int a, int b) {
        return a+b;
    }

    @Override
    public double puissance(double nombre, double exposant) {
        return Math.pow(exposant, nombre);
    }
}
