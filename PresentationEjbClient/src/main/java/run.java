import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.util.Properties;

public class run {




    public static void main(String[] args) {
        Properties p = new Properties();
        p.put(Context.PROVIDER_URL,  "http-remoting://127.0.0.1:8080");
        p.put(Context.SECURITY_PRINCIPAL, "elsene");
        p.put(Context.SECURITY_CREDENTIALS, "elsene7");
        p.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        p.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
        p.put("jboss.naming.client.ejb.context", true);
        try {
            Context ctx =new InitialContext(p);
            System.out.println(ctx.getClass());
            CalculInterface calculInterface = (CalculInterface) ctx.lookup("PresentationEjbEAR-1.0/PresentationEjb-0.0.1-SNAPSHOT/Calcul!CalculInterface");

            System.out.println(calculInterface.ajouter(4,4));
            System.out.println(calculInterface.puissance(4,4));

            MangerInterface mangerInterface = (MangerInterface) ctx.lookup("PresentationEjbEAR-1.0/PresentationEjb-0.0.1-SNAPSHOT/Manger!MangerInterface");

            System.out.println(mangerInterface.mangerDehors().getNom() + "  " +  mangerInterface.mangerDehors().getPrix() + "  " + mangerInterface.mangerDehors().getDehors() );

        } catch (NamingException e) {
            e.printStackTrace();
        }
    }


}
